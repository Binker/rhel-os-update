# RHEL OS update

### Ansible Playbook OS update  
USAGE: `$ ansible-playbook -i <fqdn1>,<fqdn2>,<fqdn...>, ospatch.yml`  

## WARNING:
This Playbook utilizes 'yum-utils', and specifically the 'needs-restarting' command.  
If there is changes to things like dbus, glibc, systemd, linux-firmware or the kernel, the server reboots.  
